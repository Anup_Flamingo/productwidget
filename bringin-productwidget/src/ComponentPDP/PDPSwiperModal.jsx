import React, { useState, useEffect, useRef, createRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faCircleCheck,
  faPlay,
  faVolumeHigh,
  faVolumeMute,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { VIDEO_URL_PDP } from "../configPDP";
import { register } from "swiper/element/bundle";
import axios from "axios";

import Modal from "react-modal";

const customStylesPDP = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
    zIndex: 10000,
    overflow: "hidden",
    height: "-webkit-fill-available",
  },
  content: {
    top: "0%",
    left: "0%",
    right: "0%",
    bottom: "0%",
    padding: 0,
    backgroundColor: "transparent",
    border: "1ps solid transparent",
    overflow: "hidden",
    height: "100%",
    zIndex: 10,
  },
};

const productModalStylePDP = {
  products: {
    height: "fit-content",
    padding: "8px",
    cursor: "pointer",
    borderRadius: "5px",
    background: "rgba(0,0,0,0.7)",
    color: "white",
    fontSize: "16px",
    display: "flex",
    gap: "10px",
  },
};

register();

Modal.setAppElement("#productWidgetRoot");

function PDPSwiperModal(prop) {
  const {
    isModalOpenPDP,
    productSliderId,
    setIsModalOpenPDP,
    productVideoResponsePDP,
    handleCloseModalPDP,
    viewportHeigthPDP,
    controlsVideoPDPRef,
    currentIndexPDP,
    setCurrentIndexPDP,
    trackEvent,
    setShowThumbnailPDP,
    descriptionBoxPDPRef,
    currentVideoPDP,
    setCurrentVideoPDP,
    pdpCheckoutEnabled,
    pdpAddToCartEnabled,
    pdpCheckoutType,
  } = prop;

  const [selectedProductIndexPDP, setSelectedProductIndexPDP] = useState(0);
  const [selectedVariantIndexPDP, setSelectedVariantIndexPDP] = useState(0);
  const [moreDesPDP, setMoreDesPDP] = useState(false);
  const [varinatSelectedPDP, setVariantSelectedPDP] = useState(null);
  const [toggleDescriptionPDP, setToggleDescriptionPDP] = useState(false);
  const [isMutePDP, setIsMutePDP] = useState(false);
  const [cartCountPDP, setCartCountPDP] = useState(0);
  const [videoWidthPDP, setVideoWidthPDP] = useState(0);

  const ModalSwiperPDPRef = useRef();
  const ProductSwiperPDPRef = useRef();
  const SubVideoSwiperPDPRef = useRef([]);
  const childModalPDPRef = useRef(null);
  const playButtonPDPRef = useRef([]);
  const descriptionTextPDPRef = useRef(null);
  const checkoutGokwikPDPRef = useRef(null);


  if (SubVideoSwiperPDPRef.current.length === 0) {
    SubVideoSwiperPDPRef.current = productVideoResponsePDP?.map((item) =>
      createRef()
    );
    playButtonPDPRef.current = productVideoResponsePDP?.map((item) => createRef());
  }

  useEffect(() => {
    const updateVideWidth = () => {
      setVideoWidthPDP(
        SubVideoSwiperPDPRef.current[currentIndexPDP]?.current?.offsetWidth
      );
    };

    window.addEventListener("resize", updateVideWidth);

    return () => window.removeEventListener("resize", updateVideWidth);
  });

  useEffect(() => {
    setTimeout(() => {
      document.body.style.overflow = "hidden";

      if (ModalSwiperPDPRef.current) {
        // Start: Modal Swiper
        const swiperParams = {
          on: {
            init() {
              // ...
            },
            slideChangeTransitionEnd: function (event) {
              setToggleDescriptionPDP(false);
              setMoreDesPDP(false);
              setVariantSelectedPDP(null); //Variant Selected is null on Change of Video Slider
              setIsMutePDP(false); //On Change Video Is Not Muted

              if (window.innerWidth < 600) {
                ModalSwiperPDPRef.current.swiper.navigation.prevEl.style.display =
                  "block";
                ModalSwiperPDPRef.current.swiper.navigation.nextEl.style.display =
                  "block";
              }

              descriptionBoxPDPRef.current.forEach((descRef) => {
                descRef.current.classList.add("hidden");
              });

              event.el.childNodes.forEach((item, index) => {
                if (item.tagName.toUpperCase() === "SWIPER-SLIDE") {
                  if (item.classList.contains("swiper-slide-active")) {
                    setCurrentIndexPDP(index); // Use Effect is called
                  }
                }
              });

              document
                .getElementById("productSwiper")
                .classList.remove("hidden"); //Product Slider is shown
              ProductSwiperPDPRef.current.swiper.slideTo(0, false); //Slided to zeroth index
            },
          },
        };

        // now we need to assign all parameters to Swiper element
        Object.assign(ModalSwiperPDPRef.current, swiperParams);

        // and now initialize it
        ModalSwiperPDPRef.current.initialize();

        // Current Video is played and Slided to the clicked thumbanail slide
        ModalSwiperPDPRef.current.swiper.slideTo(currentIndexPDP, false);
        SubVideoSwiperPDPRef.current[currentIndexPDP].current.play();

        // Setting the width of the product slider
        if (window.innerWidth < 1024) {
          document.getElementById(
            "productSwiper"
          ).style.width = `${window.innerWidth}px`;
        } else {
          document.getElementById("productSwiper").style.width = `${
            window.innerWidth * 0.33
          }px`;
        }

        // Changing color of Next and Prev Btn

        ModalSwiperPDPRef.current.swiper.navigation.prevEl.style.color = "white";
        ModalSwiperPDPRef.current.swiper.navigation.nextEl.style.color = "white";

        // End:- Modal swiper

        //Start: -- Product Swiper
        if (ProductSwiperPDPRef.current) {
          const swiperParams = {
            breakpoints: {
              0: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
              350: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
            },
            on: {
              init() {
                // ...
              },
              slideChangeTransitionEnd: function (event) {
                let videoDetailsStr;
                event?.el?.childNodes?.forEach((item) => {
                  if (item?.dataset?.videodetails) {
                    videoDetailsStr = item.dataset.videodetails;
                  }
                });

                if (videoDetailsStr) {
                  const videoDetailsJSON = JSON.parse(videoDetailsStr);

                  // console.log("PDP Product Slider Click");
                  // console.log({
                  //   store: window.location.hostname,
                  //   productId : productSliderId,
                  //   videoId: videoDetailsJSON?.id,
                  //   videoTitle: videoDetailsJSON?.title,
                  // });
                  trackEvent("PDP Product Slider Click", {
                    props: {
                      store: window.location.hostname,
                      productId : productSliderId,
                      videoId: videoDetailsJSON?.id,
                      videoTitle: videoDetailsJSON?.title,
                    },
                  });
                }
              },
            },
          };

          // now we need to assign all parameters to Swiper element
          Object.assign(ProductSwiperPDPRef.current, swiperParams);

          // and now initialize it
          ProductSwiperPDPRef.current.initialize();

          ProductSwiperPDPRef.current.swiper.navigation.prevEl.style.color =
            "white";
          ProductSwiperPDPRef.current.swiper.navigation.nextEl.style.color =
            "white";
          ProductSwiperPDPRef.current.swiper.navigation.nextEl.style.setProperty(
            "--swiper-navigation-size",
            "12px"
          );
          ProductSwiperPDPRef.current.swiper.navigation.prevEl.style.setProperty(
            "--swiper-navigation-size",
            "12px"
          );
          ProductSwiperPDPRef.current.swiper.navigation.prevEl.style.fontWeight =
            "bold";
          ProductSwiperPDPRef.current.swiper.navigation.nextEl.style.fontWeight =
            "bold";
        }
        //End: -- Product Swiper

        //Start:-- Setting the Video Width
        SubVideoSwiperPDPRef?.current[currentIndexPDP]?.current?.addEventListener(
          "loadedmetadata",
          () => {
            if (
              SubVideoSwiperPDPRef?.current[currentIndexPDP]?.current?.offsetWidth
            ) {
              const width =
                SubVideoSwiperPDPRef.current[currentIndexPDP].current.offsetWidth;
              setVideoWidthPDP(width);
            }
          }
        );
        // End:-- Setting the Video Width
      }
    }, 0);
  }, []);

  useEffect(() => {
    const updateVideWidth = () => {
      setVideoWidthPDP(
        SubVideoSwiperPDPRef.current[currentIndexPDP]?.current?.offsetWidth
      );
    };

    window.addEventListener("resize", updateVideWidth);

    return () => window.removeEventListener("resize", updateVideWidth);
  });

  useEffect(() => {
    // Playing the current window and Pausing rest
    if (currentIndexPDP !== null) {
      // Setting the video
      const currentVid = productVideoResponsePDP[currentIndexPDP];
      setCurrentVideoPDP({ ...currentVid });

      // Hiding the play button;
      playButtonPDPRef?.current[currentIndexPDP]?.current?.classList?.add("hidden");

      SubVideoSwiperPDPRef.current.forEach((video, index) => {
        if (index === currentIndexPDP) {
          video.current?.play();
        } else {
          if (video.current) video.current.currentTime = 0;
          video.current?.pause();
        }
      });

      if (SubVideoSwiperPDPRef?.current[currentIndexPDP]?.current?.offsetWidth) {
        const width =
          SubVideoSwiperPDPRef.current[currentIndexPDP].current.offsetWidth;
        setVideoWidthPDP(width);
      }

      ProductSwiperPDPRef.current?.swiper?.updateSlides(); //Update the slides
    }
  }, [currentIndexPDP]);

  useEffect(() => {
    if (currentVideoPDP) {
      // Plausible
      // console.log("PDP Video Click");
      // console.log({
      //   store: window.location.hostname,
      //   productId : productSliderId,
      //   videoId: currentVideoPDP?.id,
      //   videoTitle: currentVideoPDP?.title,
      // });
      trackEvent("PDP Video Click", {
        props: {
          store: window.location.hostname,
          productId: productSliderId,
          videoId: currentVideoPDP?.id,
          videoTitle: currentVideoPDP?.title,
        },
      });
    }
  }, [currentVideoPDP]);

  useEffect(() => {
    if (varinatSelectedPDP) {
      const currentProduct =
        productVideoResponsePDP[currentIndexPDP].products[selectedProductIndexPDP];
      // console.log("PDP Add to Cart");
      // console.log({
      //   store: window.location.hostname,
      //   productId : productSliderId,
      //   currentProductId: currentProduct?.product_id,
      //   productName: currentProduct?.title,
      //   sku: currentProduct?.variants[selectedVariantIndexPDP]?.sku,
      // });
      trackEvent("PDP Add to cart", {
        props: {
          store: window.location.hostname,
          productId : productSliderId,
          currentProductId: currentProduct?.product_id,
          productName: currentProduct?.title,
          sku: currentProduct?.variants[selectedVariantIndexPDP]?.sku,
        },
      });
    }
  }, [varinatSelectedPDP]);

  useEffect(() => {
    if (!moreDesPDP && descriptionTextPDPRef.current) {
      // descriptionTextPDPRef.current.scroll({
      //   top: 0,
      //   behavior: 'smooth'
      // })
    }
  }, [moreDesPDP]);


  const handleOpenDescriptionPDP = (event, index) => {
    event.preventDefault();
    setSelectedProductIndexPDP(index);
    if (window.innerWidth < 600) {
      ModalSwiperPDPRef.current.swiper.navigation.prevEl.style.display = "none";
      ModalSwiperPDPRef.current.swiper.navigation.nextEl.style.display = "none";
    }
    document.getElementById("productSwiper").classList.add("hidden");
    descriptionBoxPDPRef.current.forEach((descRef, i) => {
      if (i === currentIndexPDP) {
        descRef.current.classList.remove("hidden");
        descRef.current.style.width = `${SubVideoSwiperPDPRef.current[currentIndexPDP].current.clientWidth}px`;
      } else {
        descRef.current.classList.add("hidden");
      }
    });

    const currentProduct = productVideoResponsePDP[currentIndexPDP]?.products[index];
    // console.log("PDP Product Detail Open");
    // console.log({
    //   store: window.location.hostname,
    //   productId : productSliderId,
    //   videoId: currentVideoPDP?.id,
    //   videoTitle: currentVideoPDP?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[0]?.sku,
    // });
    trackEvent("PDP Product Detail Open", {
      props: {
        store: window.location.hostname,
        productId : productSliderId,
        videoId: currentVideoPDP?.id,
        videoTitle: currentVideoPDP?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[0]?.sku,
      },
    });
  };

  const handleCloseDescPDP = (event) => {
    event.preventDefault();

    if (window.innerWidth < 600) {
      ModalSwiperPDPRef.current.swiper.navigation.prevEl.style.display = "block";
      ModalSwiperPDPRef.current.swiper.navigation.nextEl.style.display = "block";
    }

    descriptionBoxPDPRef.current[currentIndexPDP].current.classList.add("hidden");
    document.getElementById("productSwiper").classList.remove("hidden");
    setSelectedVariantIndexPDP(0); //Variant is set to default index 0
    setVariantSelectedPDP(null); // Variant is set to null
    setToggleDescriptionPDP(false); //Description Box is Set to Default Mode
  };

  const handleVariantChangePDP = (event, variant, index) => {
    event.target.checked = true;
    setSelectedVariantIndexPDP(index);
  };

  const handleAddToCartPDP = (event, index) => {
    event.preventDefault();
    setToggleDescriptionPDP(true);
    const variantId =
      productVideoResponsePDP?.[currentIndexPDP]?.products[selectedProductIndexPDP]
        ?.variants[0]?.id;
    document.getElementById(`btnradio_${index}_${variantId}`).checked = true;
  };

  const handleDonePDP = (index) => {
    try {
      const variant =
        productVideoResponsePDP[index]?.products[selectedProductIndexPDP]?.variants[
          selectedVariantIndexPDP
        ];
      const payload = {
        id: variant?.variant_id,
        quantity: 1,
      };

      // Start:-- Testing Mode
      // setToggleDescriptionPDP(false);
      // setVariantSelectedPDP({ ...variant });
      // End:-- Testing Mode

      // Start: Production Mode
      axios
        .post("/cart/add.js", payload)
        .then((res) => {
          if (res?.status === 200) {
            const count = res.data.quantity;
            count && setCartCountPDP(count);
            setToggleDescriptionPDP(false);
            setVariantSelectedPDP({ ...variant });
          }
        })
        .catch((err) => {});
      // End: Production Mode
      
    } catch (err) {}
  };

  const handleMutePDP = (event, index) => {
    event.preventDefault();
    try {
      if (isMutePDP) {
        // console.log("PDP Video Unmuted");
        // console.log({
        //   store: window.location.hostname,
        //   productId : productSliderId,
        //   videoId: currentVideoPDP?.id,
        //   videoTitle: currentVideoPDP.title,
        // });
        trackEvent("PDP Video Unmuted", {
          props: {
            store: window.location.hostname,
            productId : productSliderId,
            videoId: currentVideoPDP?.id,
            videoTitle: currentVideoPDP.title,
          },
        });
      } else {
        // console.log("PDP Video Muted");
        // console.log({
        //   store: window.location.hostname,
        //   productId : productSliderId,
        //   videoId: currentVideoPDP?.id,
        //   videoTitle: currentVideoPDP.title,
        // });
        trackEvent("PDP Video Muted", {
          props: {
            store: window.location.hostname,
            productId : productSliderId,
            videoId: currentVideoPDP?.id,
            videoTitle: currentVideoPDP.title,
          },
        });
      }
      setIsMutePDP((prev) => !prev);
      SubVideoSwiperPDPRef.current[index].current.muted =
        !SubVideoSwiperPDPRef.current[index].current.muted;
    } catch (err) {}
  };

  const handlePausePlayVideoPDP = (index) => {
    const isPaused = SubVideoSwiperPDPRef.current[currentIndexPDP].current.paused;
    if (isPaused) {
      SubVideoSwiperPDPRef.current[currentIndexPDP].current.play();
      playButtonPDPRef.current[index].current.classList.add("hidden");
      // console.log("PDP Video Play");
      // console.log({
      //   store: window.location.hostname,
      //   productId : productSliderId,
      //   videoId: currentVideoPDP?.id,
      //   videoTitle: currentVideoPDP?.title,
      // });
      trackEvent("PDP Video Play", {
        props: {
          store: window.location.hostname,
          productId : productSliderId,
          videoId: currentVideoPDP?.id,
          videoTitle: currentVideoPDP?.title,
        },
      });
    } else {
      SubVideoSwiperPDPRef.current[currentIndexPDP].current.pause();
      // console.log("PDP Video Pause");
      // console.log({
      //   store: window.location.hostname,
      //   productId : productSliderId,
      //   videoId: currentVideoPDP?.id,
      //   videoTitle: currentVideoPDP?.title,
      // });
      trackEvent("PDP Video Pause", {
        props: {
          store: window.location.hostname,
          productId : productSliderId,
          videoId: currentVideoPDP?.id,
          videoTitle: currentVideoPDP?.title,
        },
      });
      playButtonPDPRef.current[index].current.classList.remove("hidden");
    }
  };

  const handleRedirectProductDespcriptionPagePDP = () => {
    const currentProduct =
      productVideoResponsePDP[currentIndexPDP].products[selectedProductIndexPDP];
    // console.log("PDP Slider Product Redirect");
    // console.log({
    //   store: window.location.hostname,
    //   productId : productSliderId,
    //   videoId: currentVideoPDP?.id,
    //   videoTitle: currentVideoPDP?.title,
    //   currentProductId: currentProduct?.id,
    //   productName: currentProduct?.title,
    //   sku: currentProduct?.variants[selectedVariantIndexPDP]?.sku,
    // });
    trackEvent("PDP Slider Product Redirect", {
      props: {
        store: window.location.hostname,
        productId : productSliderId,
        videoId: currentVideoPDP?.id,
        videoTitle: currentVideoPDP?.title,
        currentProductId: currentProduct?.id,
        productName: currentProduct?.title,
        sku: currentProduct?.variants[selectedVariantIndexPDP]?.sku,
      },
    });
  };

  return (
    <Modal
      isOpen={isModalOpenPDP}
      onRequestClose={handleCloseModalPDP}
      style={customStylesPDP}
    >
      <section
        style={{ height: `${viewportHeigthPDP}px` }}
        ref={childModalPDPRef}
        className="relative w-full xl:w-[900px]  m-auto"
        id="childModalSwiper"
      >
        <swiper-container
          ref={ModalSwiperPDPRef}
          slides-per-view={"1"}
          spaceBetween={0}
          navigation="true"
          style={{ height: `${viewportHeigthPDP}px` }}
          init="false"
        >
          {productVideoResponsePDP?.map((video, index) => {
              return (
                <swiper-slide key={video.id}>
                  <div className="flex flex-col justify-center h-full items-center relative">
                    <article className="absolute w-full flex justify-center text-white top-[1%] z-10">
                      {/* <div className='flex justify-between font-bold w-[40%] md:w-[50%] lg:w-[80%] 3xl:[95%] text-lg 2xl:text-3xl'> */}
                      <div
                        ref={controlsVideoPDPRef.current[index]}
                        className="flex justify-between font-bold px-2  text-lg 2xl:text-3xl"
                        style={{ width: `${videoWidthPDP?videoWidthPDP:"100"}px` }}
                      >
                        <div
                          className="cursor-pointer w-12 h-12 rounded-full bg-black/20 flex justify-center items-center"
                          onClick={(event) => handleMutePDP(event, index)}
                        >
                          {isMutePDP ? (
                            <FontAwesomeIcon icon={faVolumeMute} />
                          ) : (
                            <FontAwesomeIcon icon={faVolumeHigh} />
                          )}
                        </div>
                        <div
                          className="cursor-pointer w-12 h-12 rounded-full bg-black/20 flex justify-center items-center"
                          onClick={handleCloseModalPDP}
                        >
                          <FontAwesomeIcon icon={faXmark} />
                        </div>
                      </div>
                    </article>

                    <video
                      name={video?.title}
                      autoPlay={false}
                      ref={SubVideoSwiperPDPRef.current[index]}
                      playsInline
                      className="h-screen object-contain"
                      preload="meta"
                      onClick={() => handlePausePlayVideoPDP(index)}
                    >
                      <source
                        src={`${VIDEO_URL_PDP}/${video.video_url}`}
                        type="video/mp4"
                      />
                    </video>
                    {/* <article ref={descriptionBoxPDPRef} style={{width:"70%"}} className='bg-black/70 absolute bottom-0 text-sm translate-x-20 rounded-t-xl pt-5 pb-12 pl-4 pr-4 text-white  descriptionBoxModal'> */}
                    <article className="w-full max-h-[75vh] flex justify-center absolute bottom-0 xl:bottom-[0%] 2xl:bottom-[0%] 3xl:bottom-[0%] text-sm text-white  ">
                      {/* <div ref={descriptionBoxPDPRef.current[index]} className='hidden w-2/5 sm:w-[46%] md:w-[58%] lg:w-[70%] xl:w-[64%] 2xl:w-[55%] 3xl:w-[70%] bg-black/80 h-full rounded-t-2xl pt-4  pb-20 px-4'> */}
                      <div
                        ref={descriptionBoxPDPRef.current[index]}
                        className="hidden w-2/5 bg-black/80 rounded-t-2xl pt-4  pb-[30px] px-4"
                      >
                        <div className="flex justify-between pb-2 border-b-2 border-white  pr-3 font-bold">
                          <div></div>
                          <div className="tracking-wide text-lg">Shop</div>
                          <button
                            className="cursor-pointer bg-transparent text-white text-xl"
                            onClick={handleCloseDescPDP}
                          >
                            <FontAwesomeIcon icon={faXmark} />
                          </button>
                        </div>
                        <a
                          className="w-full"
                          href={`/products/${video?.products[selectedProductIndexPDP]?.handle}`}
                          onClick={handleRedirectProductDespcriptionPagePDP}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <div className="flex items-center gap-2 text-sm pt-2 font-bold">
                            <img
                              src={
                                video?.products[selectedProductIndexPDP]?.images[0]
                                  ?.src
                              }
                              alt={video?.products[selectedProductIndexPDP]?.title}
                              className="rounded"
                              width="50px"
                              height="50px"
                            />
                            <div>
                              {video?.products[selectedProductIndexPDP]?.title}{" "}
                              <br />
                              {`₹ ${video?.products[selectedProductIndexPDP]?.variants[selectedVariantIndexPDP]?.price}`}
                            </div>
                          </div>
                        </a>
                        <section
                          className="transition  duration-150 ease-out"
                          style={{
                            display: toggleDescriptionPDP ? "none" : "block",
                          }}
                        >
                          <div className="pt-2 text-sm">
                            <div className=" font-bold mb-2">Description</div>
                            <div
                              ref={descriptionTextPDPRef}
                              className={`bringinDescriptionBox transition-height text-white duration-200 ease-linear  text-justify
                              ${
                                moreDesPDP
                                  ? "max-h-52 overflow-y-scroll"
                                  : "max-h-16 overflow-y-hidden"
                              }`}
                              dangerouslySetInnerHTML={{
                                __html:
                                  video?.products[selectedProductIndexPDP]
                                    ?.description,
                              }}
                            ></div>
                          </div>
                          <div
                            className="text-center cursor-pointer mt-4 font-bold hover:text-white/80"
                            onClick={() => setMoreDesPDP((prev) => !prev)}
                          >
                            {moreDesPDP ? "Less" : "More"}
                          </div>
                          {varinatSelectedPDP ? (
                            <div className="w-full flex gap-4">
                              <a
                                className={`${
                                  pdpCheckoutEnabled === "true"
                                    ? "w-1/2"
                                    : "w-full"
                                }`}
                                onClick={handleRedirectProductDespcriptionPagePDP}
                                href="/cart"
                              >
                                <button
                                  id="bringinAddedToCart"
                                  className="w-full bg-white text-black   mt-2 mb-2 hover:bg-white/90"
                                >
                                  <FontAwesomeIcon
                                    color="#198754"
                                    icon={faCircleCheck}
                                  />{" "}
                                  Added to Cart
                                </button>
                              </a>
                              {pdpCheckoutType === "gokwik" ? (
                                <div className="gokwik-checkout mb-0 w-1/2">
                                  <button
                                    id="bringinCheckout"
                                    type="button"
                                    className="w-full  mt-2 mb-2 relative"
                                    ref={checkoutGokwikPDPRef}
                                    // onClick= "onCheckoutClick(this)"
                                    onClick={() =>
                                      window.onCheckoutClick(
                                        checkoutGokwikPDPRef.current
                                      )
                                    }
                                  >
                                    <span className="">
                                      <span>
                                        Checkout{" "}
                                        <FontAwesomeIcon
                                          icon={faCartShopping}
                                        />{" "}
                                        <span
                                          id="cartCountBadge"
                                          style={{
                                            top: "-13px",
                                            right: "-12px",
                                          }}
                                          className="absolute rounded-full  w-7 h-7 flex justify-center items-center right-0 border border-white"
                                        >
                                          {cartCountPDP}
                                        </span>
                                      </span>
                                      <span></span>
                                    </span>
                                    <span className="pay-opt-icon hidden">
                                      {/* <img src="https://cdn.gokwik.co/v4/images/upi-icons.svg" />
                                      <img src="https://cdn.gokwik.co/v4/images/right-arrow.svg" /> */}
                                    </span>
                                    <div className="hidden">
                                      <div className="cir-loader">
                                        Loading..
                                      </div>
                                    </div>
                                  </button>
                                </div>
                              ) : (
                                <a
                                  className={`${
                                    pdpCheckoutEnabled === "true"
                                      ? "w-1/2"
                                      : "hidden"
                                  }`}
                                  href="/checkout"
                                >
                                  <button
                                    id="bringinCheckout"
                                    className="w-full  mt-2 mb-2 relative"
                                  >
                                    Checkout{" "}
                                    <FontAwesomeIcon icon={faCartShopping} />{" "}
                                    <span
                                      id="cartCountBadge"
                                      style={{
                                        top: "-13px",
                                        right: "-12px",
                                      }}
                                      className="absolute rounded-full  w-7 h-7 flex justify-center items-center right-0 border border-white"
                                    >
                                      {cartCountPDP}
                                    </span>
                                  </button>
                                </a>
                              )}
                            </div>
                          ) : !varinatSelectedPDP &&
                            pdpAddToCartEnabled === "true" ? (
                            <button
                              id="bringinAddToCart"
                              className="w-full mt-2 mb-2"
                              onClick={(event) => handleAddToCartPDP(event, index)}
                            >
                              Add to Cart
                            </button>
                          ) : (
                            ""
                          )}
                        </section>
                        <section
                          style={{
                            display: !toggleDescriptionPDP ? "none" : "block",
                          }}
                          className="mt-4"
                        >
                          <article className="flex justify-center flex-wrap gap-3">
                            {video?.products[
                              selectedProductIndexPDP
                            ]?.variants?.map((variant, idt) => {
                              return (
                                <div key={variant?.id} className="mt-2">
                                  <input
                                    type="radio"
                                    value={variant.variant_id}
                                    className="btnVariantRadio hidden"
                                    name={`btnradio_${index}`}
                                    id={`btnradio_${index}_${variant.id}`}
                                    onChange={(event) =>
                                      handleVariantChangePDP(event, variant, idt)
                                    }
                                  />
                                  <label
                                    className="btnVariant cursor-pointer"
                                    htmlFor={`btnradio_${index}_${variant.id}`}
                                  >
                                    {variant.title}
                                  </label>
                                </div>
                              );
                            })}
                          </article>
                          <div className="flex justify-center mt-3">
                            <button
                              className=" w-fit text-black p-2 text-base bg-white border-white rounded mt-2 mb-2 px-5 py-2  hover:bg-white/90"
                              onClick={() => handleDonePDP(index)}
                            >
                              Done
                            </button>
                          </div>
                        </section>
                      </div>
                    </article>

                    <div
                      ref={playButtonPDPRef.current[index]}
                      className="hidden text-8xl xl:text-[10rem]  text-white/40 absolute top-2/4  left-2/4 translate-x-[-50%] translate-y-[-50%]"
                      onClick={() => handlePausePlayVideoPDP(index)}
                    >
                      <FontAwesomeIcon icon={faPlay} />
                    </div>
                  </div>
                </swiper-slide>
              );
            })}
        </swiper-container>
        <article
          id="productSwiper"
          style={{
            zIndex: 1,
            transform: "translateX(-50%)",
            width: `${videoWidthPDP}px`,
          }}
          className="absolute bottom-[2%] left-2/4 "
        >
          <swiper-container
            className="mySwiper"
            ref={ProductSwiperPDPRef}
            navigation="true"
            centered-slides="true"
            init="false"
          >
            {productVideoResponsePDP[currentIndexPDP]?.products?.map(
              (prod, index) => {
                const obj = {
                  id: productVideoResponsePDP[currentIndexPDP]?.id,
                  title: productVideoResponsePDP[currentIndexPDP]?.title,
                };
                return (
                  <swiper-slide
                    style={productModalStylePDP.products}
                    key={prod?.id}
                    data-videoDetails={JSON.stringify(obj)}
                    onClick={(event) => handleOpenDescriptionPDP(event, index)}
                  >
                    <img
                      className="w-[60px] h-[60px] rounded object-cover "
                      alt={prod?.images[0]?.alt}
                      src={prod?.images[0]?.src}
                    />
                    <div className="flex flex-col justify-between w-[60%] xxlg:w-[63%] 5xl:w-[70%] text-[14px] ">
                      <div className="prodTitle overflow-hidden whitespace-nowrap text-ellipsis ">
                        {prod?.title}
                      </div>
                      <div>{`₹ ${prod?.variants[0]?.price} `}</div>
                      <div>Shop Now {`>`}</div>
                    </div>
                  </swiper-slide>
                );
              }
            )}
          </swiper-container>
        </article>
      </section>
    </Modal>
  );
}

export default PDPSwiperModal;
