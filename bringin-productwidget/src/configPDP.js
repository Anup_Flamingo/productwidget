export const PRODUCT_URL_PDP = "https://bringin-shoppable.lyxelandflamingotech.in/api/popup-video";
export const VIDEO_URL_PDP =  "https://bringin-shoppable.sgp1.cdn.digitaloceanspaces.com";

export const BRINGIN_URL_PDP = "https://apps.shopify.com/bringin?utm_source=powered_by-widget";
export const Thumbnail_URL_PDP = "https://bringin-shoppable.sgp1.cdn.digitaloceanspaces.com";

// Start: Testing Mode  
// export const PLAUSIBLE_DOMAIN_PDP = "bringin-reviews.test";
// End: Testing Mode

// Start: Production Mode
export const PLAUSIBLE_DOMAIN_PDP = "bringin-shoppable.lyxelandflamingotech.in";
// End: Production Mode
export const API_HOST_PDP = "https://analytics.lyxelandflamingotech.in";
