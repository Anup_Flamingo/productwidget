import { useEffect, useState, useRef, createRef } from 'react'
import './AppPDP.css'
import WaitForElementToDisplay from './MethodsPDP/MountUIPDP';
import PDPSwiperModal from './ComponentPDP/PDPSwiperModal';
import { PRODUCT_URL_PDP, BRINGIN_URL_PDP, Thumbnail_URL_PDP, VIDEO_URL_PDP, PLAUSIBLE_DOMAIN_PDP, API_HOST_PDP } from "./configPDP";
import { register } from "swiper/element/bundle";
import Plausible from "plausible-tracker";
import verge from "verge";
import axios from 'axios';

// Testing Mode
// import './DummyStyling.css'
// Testing Mode



register();

const { trackEvent, enableAutoPageviews } = Plausible({
  domain: PLAUSIBLE_DOMAIN_PDP,
  apiHost: API_HOST_PDP,
  // Start: Testing Mode
  // trackLocalhost: true,
  // End: Testing Mode
});

function AppPDP() {
  const [mountedPDP, setMountedPDP] = useState(false);
  const [viewportHeigthPDP, setViewportHeightPDP] = useState(verge?.viewportH());
  const [productVideoResponsePDP, setProductVideResponsePDP] = useState([]);
  const [isModalOpenPDP, setIsModalOpenPDP] = useState(false);
  const [currentVideoPDP, setCurrentVideoPDP] = useState(null);
  const [currentIndexPDP, setCurrentIndexPDP] = useState(0);
  const [showThumbnailPDP, setShowThumbnailPDP] = useState(true);
  const [draggingPDP, setDraggingPDP] = useState(false);
  // const [position, setPosition] = useState(initialPosition);

  
  // Start: Testing Mode
  const [productSliderId, setProductSliderId] = useState("7766043328666"); //2
  // const [productSliderId, setProductSliderId] = useState("7089042751654"); //5
  // End: Testing Mode

  // Start: Production Mode
  // const [productSliderId, setProductSliderId] = useState(null);
  // End: Production Mode
  const [pdpCheckoutEnabled, setPdpCheckoutEnabled] = useState("true");
  const [pdpAddToCartEnabled, setPdpAddToCartEnabled] = useState("true");
  const [pdpCheckoutType, setPdpCheckoutType] = useState("shopify");

  const swiperElPDPRef = useRef();
  const VideoSwiperPDPRef = useRef([]);
  const controlsVideoPDPRef = useRef([]);
  const descriptionBoxPDPRef = useRef([]);

  useEffect(()=>{
    WaitForElementToDisplay(
      ".maProductWidget",
      function () {
        setMountedPDP(true);
      },
      100,
      5000
    );

    if (swiperElPDPRef.current) {
      const swiperParams = {
        breakpoints: {
          0: {
            slidesPerView: 2,
            // spaceBetween: "auto",
          },
          570: {
            slidesPerView: 2,
            // spaceBetween: 20,
          },
          850: {
            slidesPerView: 3,
            // spaceBetween: 20,
          },
          1110: {
            slidesPerView: 4,
            // spaceBetween: 10,
          },
          1500: {
            slidesPerView: 5,
          },
          1920: {
            slidesPerView: 6,
          },
          2550: {
            slidesPerView: 7,
          },
        },
        on: {
          init() {
            // ...
          },
          slideChangeTransitionEnd: function () {
            // console.log("PDP Slider Click");
            // console.log({
            //   store: window.location.hostname,
            //   productId: productSliderId,
            // });
            trackEvent("PDP Slider Click", {
              props: {
                store: window.location.hostname,
                productId: productSliderId,
              },
            });
          },
        },
      };

      // now we need to assign all parameters to Swiper element
      Object.assign(swiperElPDPRef.current, swiperParams);

      // and now initialize it
      swiperElPDPRef.current.initialize();

      if (
        document
          .querySelector("#parentSwiperSection  swiper-container")
          ?.shadowRoot?.querySelector(".swiper-button-prev") &&
        document
          .querySelector("#parentSwiperSection  swiper-container")
          ?.shadowRoot?.querySelector(".swiper-button-next")
      ) {
        document
          .querySelector("#parentSwiperSection  swiper-container")
          .shadowRoot.querySelector(".swiper-button-prev").style.color =
          "white";
        document
          .querySelector("#parentSwiperSection  swiper-container")
          .shadowRoot.querySelector(".swiper-button-next").style.color =
          "white";
      }
    }

    visualViewport.addEventListener("resize", (event) => {
      setViewportHeightPDP(verge?.viewportH());
    });
  },[])

  useEffect(() => {
    if (mountedPDP) {
      setProductSliderId(document.querySelector(".maProductWidget").dataset.productid);
      setPdpCheckoutEnabled(
        document.querySelector(".maProductWidget").dataset.checkoutenabled
      );
      setPdpAddToCartEnabled(
        document.querySelector(".maProductWidget").dataset.addtocartenabled
      );
      setPdpCheckoutType(
        document.querySelector(".maProductWidget").dataset.checkouttype
      );
    }
  }, [mountedPDP]);

  useEffect(() => {
    if (productSliderId) {
      axios.get(`${PRODUCT_URL_PDP}/${productSliderId}`).then((res) => {

        setProductVideResponsePDP([...res.data]);
        // console.log("View PDP");
        // console.log({
        //   store: window.location.hostname,
        //   productId: productSliderId,
        // });
        enableAutoPageviews();
        trackEvent("View PDP", {
          props: {
            store: window.location.hostname,
            productId: productSliderId,
          },
        });

        VideoSwiperPDPRef.current = res.data?.map((video) =>createRef());

        descriptionBoxPDPRef.current = res.data?.map((item) => createRef());

        controlsVideoPDPRef.current = res.data?.map((item) => createRef());
      });
    }
  }, [productSliderId]);

  const handleOpenModalPDP = (video, index) => {
    setCurrentIndexPDP(index);
    // Pausing all the background Thumbnail Video
    VideoSwiperPDPRef.current.forEach((videoRef) => {
      videoRef.current?.pause();
    });
    swiperElPDPRef.current.style.zIndex = -1;

    if (
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-prev") &&
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-next")
    ) {
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-prev").style.display = "none";
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-next").style.display = "none";
    }

    setIsModalOpenPDP(true);
  };

  const handleCloseModalPDP = () => {
    VideoSwiperPDPRef.current.forEach((videoRef) => {
      videoRef.current?.play();
    });
    swiperElPDPRef.current.style.zIndex = 1;
    if (
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-prev") &&
      document
        .querySelector("#parentSwiperSection  swiper-container")
        ?.shadowRoot?.querySelector(".swiper-button-next")
    ) {
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-prev").style.display =
        "block";
      document
        .querySelector("#parentSwiperSection  swiper-container")
        .shadowRoot.querySelector(".swiper-button-next").style.display =
        "block";
    }

    setCurrentIndexPDP(null);
    setCurrentVideoPDP(null);
    setIsModalOpenPDP(false);
    document.body.style.overflow = "auto";
  };


  return (
    <section id="parentSwiperSection">
    <swiper-container
      ref={swiperElPDPRef}
      init="false"
      navigation="true"
      space-between={10}
      // centered-slides="true"
    >
      {
        productVideoResponsePDP?.map((video, index) => {
          return (
            <swiper-slide
              style={{ display: "flex", justifyContent: "center" }}
              key={video.id}
            >
              <video
                className="bringinvideo"
                ref={VideoSwiperPDPRef.current[index]}
                autoPlay
                loop
                muted
                playsInline
                preload="auto"
                onClick={() => handleOpenModalPDP(video, index)}
              >
                <source
                  src={`${Thumbnail_URL_PDP}/${video.thumbnail}`}
                  type="video/mp4"
                />
              </video>
            </swiper-slide>
          );
        })}
    </swiper-container>
    <div className="text-right pr-2">
      <a
        className="text-black"
        href={BRINGIN_URL_PDP}
        target="_blank"
        rel="noreferrer"
      >
        Powered by Bringin
      </a>
    </div>
    {isModalOpenPDP ? (
        <PDPSwiperModal
          isModalOpenPDP={isModalOpenPDP}
          productSliderId={productSliderId}
          handleCloseModalPDP={handleCloseModalPDP}
          setIsModalOpenPDP={setIsModalOpenPDP}
          productVideoResponsePDP={productVideoResponsePDP}
          viewportHeigthPDP={viewportHeigthPDP}
          controlsVideoPDPRef={controlsVideoPDPRef}
          currentIndexPDP={currentIndexPDP}
          setCurrentIndexPDP={setCurrentIndexPDP}
          trackEvent={trackEvent}
          setShowThumbnailPDP={setShowThumbnailPDP}
          pdpCheckoutEnabled={pdpCheckoutEnabled}
          pdpAddToCartEnabled={pdpAddToCartEnabled}
          pdpCheckoutType={pdpCheckoutType}
          currentVideoPDP={currentVideoPDP}
          setCurrentVideoPDP={setCurrentVideoPDP}
          descriptionBoxPDPRef={descriptionBoxPDPRef}
        />
      ) : (
        ""
      )}
  </section>
  )
}

export default AppPDP
