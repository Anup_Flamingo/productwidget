import React from 'react'
import ReactDOM from 'react-dom/client'
import AppPDP from './AppPDP.jsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('productWidgetRoot')).render(
  <React.StrictMode>
    <AppPDP />
  </React.StrictMode>,
)
